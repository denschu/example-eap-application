example-eap-application: Using Multiple Java EE 6 Technologies Deployed as an EAR
==============================================================================================

## Used command to generate app
mvn archetype:generate -DarchetypeGroupId=org.jboss.archetype.eap -DarchetypeArtifactId=jboss-javaee6-webapp-ear-archetype -DarchetypeVersion=6.4.0.GA

## Build and Deploy to local instance
mvn clean install jboss-as:deploy

## Undeploy from local instance
mvn jboss-as:undeploy

## Build and create Docker Image
mvn clean package -Pdocker

## Run container
docker run --name="example-eap-application" --rm=true -p 8080:8080 -p 9990:9990 -p 9999:9999 example-eap-application

The application will be running at the following URL: <http://localhost:8080/example-eap-application-web>.

## Access application in Docker Virtualbox
http://${docker-machine ip default}:8080/example-eap-application-web
